Tarzan Transactions
=======================
This folder mainly contains all the transactions of the Tarzan framework.
* `__init__.py` - This is the initialization file
* `maltego/` - This contains the maltego transaction module of Tarzan
